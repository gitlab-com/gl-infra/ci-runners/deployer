FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

RUN set -x && \
    apt-get update && \
    apt-get install -y --no-install-recommends pipenv openssh-client jq curl parallel apt-transport-https ca-certificates gnupg git && \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
    curl -s https://apt.releases.hashicorp.com/gpg | apt-key --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg add - && \
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com focal main" | tee -a /etc/apt/sources.list.d/hashicorp.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends google-cloud-sdk vault && \
    rm -rf /var/lib/apt/lists/*

COPY Pipfile Pipfile.lock /
RUN pipenv install --system --deploy --ignore-pipfile && \
    rm Pipfile Pipfile.lock

RUN adduser --disabled-login --gecos "" gitlab
USER gitlab
WORKDIR /home/gitlab
