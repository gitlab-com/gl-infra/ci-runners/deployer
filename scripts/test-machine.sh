#!/usr/bin/env bash
set -eo pipefail
set -x

eval $(docker-machine env "${VM_MACHINE}")
docker info
docker version
eval $(docker-machine env --unset)
timeout 60 docker-machine ssh "${VM_MACHINE}" "hostname"
