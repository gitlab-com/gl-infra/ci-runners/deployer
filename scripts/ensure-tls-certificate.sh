#!/usr/bin/env bash
set -eo pipefail

# The name of the test machine
export VM_MACHINE=docker-machine-tls-test-vm-$((1 + RANDOM % 1000))

# Run TLS certificate generation only
# if docker+machine executor is in use
if ! grep -q 'executor = "docker+machine"' /etc/gitlab-runner/config.toml &> /dev/null; then
    echo "Docker Machine not used by runner; skipping TLS generation"
    exit 0
fi

# Ensure we get to remove the test VM if any of the steps fail
result=0

# Generate the create-machine script
./generate-create-machine.sh || result=1
chmod +x /tmp/create-machine.sh

# Execute the create and test
{ ./create-machine.sh || docker-machine regenerate-certs -f "${VM_MACHINE}" || { rm -rf /root/.docker/machine/certs && ./create-machine.sh; } ; } && ./test-machine.sh || result=1

# Force remove the test machine
timeout 45 docker-machine rm -f "${VM_MACHINE}" || { echo "no problems, machine was removed" && docker-machine ls || echo "ignore these errors"; }
exit $result
