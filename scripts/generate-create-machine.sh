#!/usr/bin/env bash
set -eo pipefail
set -x

CONFIG=${CONFIG:-/etc/gitlab-runner/config.toml}

# If `config.toml` doesn't exist, this machine was never provisioned
# simply exit
ls "${CONFIG}"

CUR_PATH=/tmp

# Generate the configs needed for creating a machine
cat "${CONFIG}" | grep MachineOptions > "${CUR_PATH}/create-machine.sh"
sed -i -e 's/[[:space:]]*MachineOptions = \[/docker-machine create --driver google /' "${CUR_PATH}/create-machine.sh"
sed -i -e "s/\"\]//" "${CUR_PATH}/create-machine.sh"
sed -i -e 's/",/ /g' "${CUR_PATH}/create-machine.sh"
sed -i -e 's/"/--/g' "${CUR_PATH}/create-machine.sh"
sed -i -e "s/$/ ${VM_MACHINE}/" "${CUR_PATH}/create-machine.sh"

